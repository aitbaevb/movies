package ab.innovo.movie.domain.model

import com.google.gson.annotations.SerializedName

data class Movie(
    @SerializedName("id") val id: Long,
    @SerializedName("original_title") val originalTitle: String,
    @SerializedName("title") val title: String,
    @SerializedName("backdrop_path") val backdropPath: String,
    @SerializedName("vote_average") val voteAverage: Double,
    @SerializedName("text") val text: String
)

data class MoviePagedResponse(
    @SerializedName("total_pages") val total: Int = 0,
    val page: Int = 1,
    val results: List<Movie>,
)