package ab.innovo.movie.domain.repository

import ab.innovo.movie.domain.model.Movie
import androidx.paging.PagingData
import io.reactivex.Flowable

interface MoviesRepository {

    fun fetchMovies(): Flowable<PagingData<Movie>>

}