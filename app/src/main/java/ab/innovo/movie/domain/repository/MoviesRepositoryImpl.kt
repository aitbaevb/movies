package ab.innovo.movie.domain.repository

import ab.innovo.movie.domain.model.Movie
import ab.innovo.movie.domain.paging.MoviesPagingSource
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava2.cachedIn
import androidx.paging.rxjava2.flowable
import io.reactivex.Flowable
import javax.inject.Inject

class MoviesRepositoryImpl @Inject constructor(
    private val pagingSource: MoviesPagingSource
): MoviesRepository {

    override fun fetchMovies(): Flowable<PagingData<Movie>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = true,
                maxSize = 30,
                prefetchDistance = 5,
                initialLoadSize = 40),
            pagingSourceFactory = { pagingSource }
        ).flowable
    }
}