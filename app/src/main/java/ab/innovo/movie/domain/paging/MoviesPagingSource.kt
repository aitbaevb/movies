package ab.innovo.movie.domain.paging

import ab.innovo.movie.domain.model.Movie
import ab.innovo.movie.domain.remote.MoviesRemoteDataSource
import androidx.paging.PagingState
import androidx.paging.rxjava2.RxPagingSource
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MoviesPagingSource @Inject constructor(
    private val remoteDataSource: MoviesRemoteDataSource
) : RxPagingSource<Int, Movie>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, Movie>> {
        val position = params.key ?: 1

        return remoteDataSource.fetchMovies(page = position)
            .subscribeOn(Schedulers.io())
            .map { toLoadResult(it.results, position, it.total) }
            .doOnError {  }
    }

    private fun toLoadResult(data: List<Movie>, position: Int, total: Int): LoadResult<Int, Movie> {
        return LoadResult.Page(
            data = data,
            prevKey = if (position == 1) null else position - 1,
            nextKey = if (position == total) null else position + 1
        )
    }

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? {
        return state.anchorPosition?.let { state.closestItemToPosition(it)?.id?.toInt() }
    }
}