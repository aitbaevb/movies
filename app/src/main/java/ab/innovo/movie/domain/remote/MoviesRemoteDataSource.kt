package ab.innovo.movie.domain.remote

import ab.innovo.movie.base.AppConstants
import ab.innovo.movie.domain.model.MoviePagedResponse
import io.reactivex.Single
import retrofit2.http.*

interface MoviesRemoteDataSource {

    @GET("3/movie/popular")
    fun fetchMovies(
        @Query("api_key") apiKey: String = AppConstants.API_KEY,
        @Query("page") page: Int
    ): Single<MoviePagedResponse>
}