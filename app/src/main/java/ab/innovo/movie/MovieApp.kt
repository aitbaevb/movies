package ab.innovo.movie

import ab.innovo.movie.di.DaggerAppComponent
import android.app.Application

class MovieApp: Application() {

    val appComponent by lazy {
        DaggerAppComponent.builder()
            .application(this)
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        appComponent.inject(this)
    }

    companion object {
        private var instance: MovieApp? = null

        fun getInstance(): MovieApp = instance!!
    }
}