package ab.innovo.movie.di.module

import ab.innovo.movie.domain.repository.MoviesRepository
import ab.innovo.movie.domain.repository.MoviesRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface RepositoryBindingModule {

    @Binds
    fun bindMoviesRepository(repository: MoviesRepositoryImpl): MoviesRepository
}