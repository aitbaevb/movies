package ab.innovo.movie.di

import ab.innovo.movie.MovieApp
import ab.innovo.movie.di.module.AppModule
import ab.innovo.movie.di.module.RepositoryBindingModule
import ab.innovo.movie.di.module.RetrofitProvidingModule
import ab.innovo.movie.di.module.ViewModelBindingModule
import ab.innovo.movie.ui.MainActivity
import android.app.Application
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        RetrofitProvidingModule::class,
        ViewModelBindingModule::class,
        RepositoryBindingModule::class
    ]
)
interface AppComponent {

    fun inject(target: MovieApp)
    fun inject(target: MainActivity)

    @Component.Builder
    interface Builder {

        @BindsInstance fun application(app: Application): Builder
        fun build(): AppComponent
    }
}