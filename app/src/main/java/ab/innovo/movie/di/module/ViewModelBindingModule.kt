package ab.innovo.movie.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import ab.innovo.movie.di.factory.InjectingViewModelFactory
import ab.innovo.movie.di.mapkey.ViewModelKey
import ab.innovo.movie.ui.MainViewModel
import androidx.lifecycle.ViewModel
import dagger.multibindings.IntoMap

@Module
interface ViewModelBindingModule {

    @Binds
    fun bindViewModelFactory(factory: InjectingViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindMainViewModel(viewModel: MainViewModel): ViewModel

}