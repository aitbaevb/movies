package ab.innovo.movie.ui

import ab.innovo.movie.MovieApp
import ab.innovo.movie.R
import ab.innovo.movie.databinding.ActivityMainBinding
import ab.innovo.movie.ui.adapter.MoviesAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainActivity: AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    @Inject lateinit var moviesAdapter: MoviesAdapter
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private var internetDisposable: Disposable? = null
    private val mainViewModel: MainViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        MovieApp.getInstance().appComponent.inject(this)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initMoviesRecyclerView()
    }

    private fun observeInternet() {
        internetDisposable = ReactiveNetwork.observeInternetConnectivity()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { isConnected: Boolean ->
                if (!isConnected)
                    Toast.makeText(this,
                        getString(R.string.error_no_internet),
                        Toast.LENGTH_SHORT).show()
                else
                    mainViewModel.compositeDisposable.add(
                        mainViewModel.fetchMovies().subscribe {
                            moviesAdapter.submitData(lifecycle, it)
                        }
                    )
            }
    }

    override fun onResume() {
        super.onResume()
        observeInternet()
    }

    override fun onPause() {
        super.onPause()
        if (internetDisposable != null && !internetDisposable!!.isDisposed) {
            internetDisposable!!.dispose()
        }
    }

    private fun initMoviesRecyclerView() {
        binding.recyclerviewMovies.adapter = moviesAdapter
    }
}