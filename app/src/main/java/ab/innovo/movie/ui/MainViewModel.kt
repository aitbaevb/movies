package ab.innovo.movie.ui

import ab.innovo.movie.base.BaseViewModel
import ab.innovo.movie.domain.model.Movie
import ab.innovo.movie.domain.repository.MoviesRepository
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.rxjava2.cachedIn
import io.reactivex.Flowable
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val moviesRepository: MoviesRepository
): BaseViewModel() {

    fun fetchMovies(): Flowable<PagingData<Movie>> {
        return moviesRepository.fetchMovies().cachedIn(viewModelScope)
    }

}