package ab.innovo.movie.ui.adapter

import ab.innovo.movie.base.AppConstants
import ab.innovo.movie.databinding.ItemMovieBinding
import ab.innovo.movie.domain.model.Movie
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import javax.inject.Inject

class MoviesAdapter @Inject constructor(

): PagingDataAdapter<Movie, MoviesAdapter.CharacterViewHolder>(CharacterComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CharacterViewHolder(
            ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int)  {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class CharacterViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Movie) = with(binding) {
            ivPoster.load(AppConstants.PHOTO_URL + item.backdropPath)
            tvTitle.text = item.title
            tvVote.text = item.voteAverage.toString()
        }
    }

    object CharacterComparator : DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie) =
            oldItem == newItem
    }
}